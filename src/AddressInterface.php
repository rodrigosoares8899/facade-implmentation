<?php


namespace Pattern;


interface AddressInterface
{

    public function addAddress($address);

}