<?php


namespace Pattern;


class User
{

    private $userPhoto;

    private $userAddress;

    private $name;

    private $accessToken;

    public function __construct(AddressInterface $userAddress, PhotoInterface $userPhoto)
    {

        $this->userAddress = $userAddress;

        $this->userPhoto = $userPhoto;

    }

    public function userPhoto(){
        return $this->userPhoto;
    }

    public function userAddress(){
        return $this->userAddress;
    }


    public function getName()
    {
        return $this->name;
    }


    public function setName($name)
    {
        $this->name = $name;
    }


    public function getAccessToken()
    {
        return $this->accessToken;
    }


    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;
    }



}