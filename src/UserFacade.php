<?php


namespace Pattern;


class UserFacade
{

    protected $user;

    public function __construct()
    {
        $userAddress = new UserAddress();

        $userPhoto = new UserPhoto();

        $this->user = new User($userAddress, $userPhoto);
    }

    public function createUser($addressData, $photoData, $userData){

        $this->user->setName($userData['name']);

        $this->user->setAccessToken($userData['accessToken']);

        $this->user->userAddress()
            ->addAddress($addressData);

        $this->user->userPhoto()
            ->addPhoto($photoData);

    }

}