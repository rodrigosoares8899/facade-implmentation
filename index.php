<?php

include_once 'bootstrap.php';

use Pattern\UserFacade;

$address = array(
    'street' => 'José Catulino',
    'disctrict' => 'Major Prates',
    'number' => 235,
    'city' => 'Montes Claros'
);

$photo = array(
    'name' => 'Rodrigo.jpg',
    'extension' => 'jpg',
    'path' => 'images/223bf23fec3990-3933$44fbbcfe3.jpg'
);

$request = array(
    'name' => 'Rodrigo',
    'accessToken' => 'ospotnth8f663bfioiw@$&$*$nc'
);

$facade = new UserFacade();

$facade->createUser($address, $photo, $request);

var_dump($facade);

?>